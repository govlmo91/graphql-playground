package kr.gracelove.graphqlplayground.akquinet.pet;

public enum PetType {
    CAT, DOG, BIRD
}
