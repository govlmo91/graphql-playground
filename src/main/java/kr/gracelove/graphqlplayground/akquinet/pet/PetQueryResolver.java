package kr.gracelove.graphqlplayground.akquinet.pet;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class PetQueryResolver implements GraphQLQueryResolver {
    private final PetRepository petRepository;

    public List<PetEntity> pets() {
        return petRepository.findAll();
    }

    public PetEntity pet(Long id) {
        return petRepository
                .findById(id)
                .orElseThrow();
    }
}
