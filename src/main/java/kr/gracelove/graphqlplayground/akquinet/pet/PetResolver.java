package kr.gracelove.graphqlplayground.akquinet.pet;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.stereotype.Component;

@Component
public class PetResolver implements GraphQLResolver<PetEntity> {

    public String name(PetEntity pet) {
        return pet.getName();
    }
}
