package kr.gracelove.graphqlplayground.akquinet.pet;

import kr.gracelove.graphqlplayground.akquinet.person.PersonEntity;
import lombok.Getter;

import javax.persistence.*;

@Entity
@Getter
public class PetEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pet_id")
    private Long id;

    @Column(name = "pet_name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private PersonEntity owner;

    @Enumerated(EnumType.STRING)
    private PetType type;


}
