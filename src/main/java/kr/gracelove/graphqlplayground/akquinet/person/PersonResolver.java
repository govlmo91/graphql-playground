package kr.gracelove.graphqlplayground.akquinet.person;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.stereotype.Component;

@Component
public class PersonResolver implements GraphQLResolver<PersonEntity> {

    public String name(PersonEntity person) {
        return person.getName();
    }
}
