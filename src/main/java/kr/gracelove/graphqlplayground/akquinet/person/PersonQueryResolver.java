package kr.gracelove.graphqlplayground.akquinet.person;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class PersonQueryResolver implements GraphQLQueryResolver {

    private final PersonRepository personRepository;

    public List<PersonEntity> persons() {
        return personRepository.findAll();
    }

    public PersonEntity person(Long id) {
        return personRepository
                .findById(id)
                .orElseThrow();
    }
}
