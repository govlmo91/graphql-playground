package kr.gracelove.graphqlplayground.akquinet.person;

import kr.gracelove.graphqlplayground.akquinet.AddressEntity;
import kr.gracelove.graphqlplayground.akquinet.pet.PetEntity;
import lombok.Getter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
public class PersonEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "person_id")
    private Long id;

    @Column(name = "person_name")
    private String name;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AddressEntity address;

    @OneToMany(cascade = CascadeType.ALL)
    private List<PetEntity> pets = new ArrayList<>();
}
