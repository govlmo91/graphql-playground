package kr.gracelove.graphqlplayground.akquinet;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Getter
public class AddressEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "address_id")
    private Long id;
    private String street;
    private String zipCode;
    private String city;

}

